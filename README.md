# Simple Article Editor

[Simple Article Editor](https://gitlab.com/mjy-hobby/webpage-features/simple-article-editor)에 대한 서버 예시



## License

제작자의 허가를 받았거나 출처를 표기한 경우에 한해

(위 사항을 어기지 않으면) 자유롭게 수정/배포/사용 가능합니다.

직·간접적인 어떤 종류의 모든 보증을 부인합니다.



## Getting Started

사용법



### Prerequisites

PHP를 지원하는 웹 서버, MySQL 서버



### Setting

MySQL이나 MariaDB 서버에서 `init.sql`을 실행해 주세요. (최초 1회)

`write.php`의 데이터베이스 연결 정보를 적절히 변경해 주세요.

프론트 부분은 [여기](https://gitlab.com/mjy-hobby/webpage-features/simple-article-editor)를 참조해 주세요!



## Deployment

적당히 복붙하고 DB 연결 정보, 이름 등을 바꿔주세요.



## Contributing

간단한 수정이라도 PR 환영합니다.

```C
if(cond)
{
    something();
}
```

줄바꿈 많이 쓰고, 인덴트는 Space 네 개로 해 주세요.



## Authors

- 맹주영 - _Initial work_ - [mjy9088](https://gitlab.com/mjy9088)



## Acknowledgment

- 기능 요청도 환영합니다!